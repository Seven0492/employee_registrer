# Main
1. Create a data structure for employees of a company. Include a person's first name, last name, age, and job title.
2. Create the three following employees:
   - John Smith, 39, Manager
   - Jane Smith, 28, Secretary
   - Fred Johnson, 34, Janitor
3. Create a variable with instances of these employees.
4. Print out each of these employee objects from the list. (tell me what the output is)
5. Create a string representation for your Employee class in the following format:
   `Name: $first_name $lastname, Age: $age, Title: $title` (this is pseudocode)
   And then rerun the program.
6. Create a data structure for a company that has a name, list of employees, and a number of employees.
7. Make it so that the "number of employees" is created from the number of employees in "list of employees".
8. Create two company objects, "Honda" and "Toyota".
9. Create a "`list_employees`" method that prints out all employee objects and run it for both company objects.

# Improving our code
1. Change "Janitor" to "Jamitor" and rerun your code. What happens?
2. Change job title to an enum to fix this. Rerun. What happens?
3. Put variables back to their original values and rerun. Everything good?
