#[derive(Debug)]
enum JobTitle {
    Manager,
    Secretary,
    Janitor,
}

impl std::fmt::Display for JobTitle {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:#?}", self)
    }
}

struct Company {
    name: String,
    employees: Vec<Employee>,
    _employee_number: usize,
}

impl Company {
    fn new(name: &str, employees: Vec<Employee>) -> Company {
        Company {
            name: name.to_string(),
            _employee_number: employees.len(),
            employees,
        }
    }

    fn _list_employees(&self) -> String {
        let mut string = String::new();

        for n in &self.employees {
            string.push_str(&format!("{}\n", n));
        }

        string
    }
}

impl std::fmt::Display for Company {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let Company {
            name, employees, ..
        } = self;

        write!(f, "Company name: {}\n\n", name,)?;

        write!(f, "Employees:\n")?;
        for employee in employees {
            write!(f, "{}\n", employee)?;
        }

        Ok(())
    }
}

struct Employee {
    first_name: String,
    last_name: String,
    age: u8,
    job_title: JobTitle,
}

impl Employee {
    fn new(first_name: &str, last_name: &str, age: u8, job_title: JobTitle) -> Employee {
        Employee {
            first_name: first_name.to_string(),
            last_name: last_name.to_string(),
            age,
            job_title,
        }
    }
}

impl std::fmt::Display for Employee {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let Employee {
            first_name,
            last_name,
            age,
            job_title,
        } = self;

        write!(
            f,
            "Name: {} {}, Age: {}, Title: {}",
            first_name, last_name, age, job_title
        )
    }
}

fn main() {
    let employee_1 = Employee::new("John", "Smith", 39, JobTitle::Manager);
    let employee_2 = Employee::new("Jane", "Smith", 28, JobTitle::Secretary);
    let employee_3 = Employee::new("Fred", "Johnson", 34, JobTitle::Janitor);

    let employees = vec![employee_1, employee_2, employee_3];

    // for n in employees {
    //     println!("{}", n);
    // }

    let company_1 = Company::new("Honda", employees);

    println!("{}", company_1);
}
